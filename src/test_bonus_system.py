from bonus_system import calculateBonuses


def test_standard():
    assert calculateBonuses("Standard", 1) == __mult(1, 0.5)
    assert calculateBonuses("Standard", 10000) == __mult(1.5, 0.5)
    assert calculateBonuses("Standard", 50000) == __mult(2, 0.5)
    assert calculateBonuses("Standard", 100000) == __mult(2.5, 0.5)
    assert calculateBonuses("Standard", 100001) == __mult(2.5, 0.5)


def test_premium():
    assert calculateBonuses("Premium", 1) == __mult(1, 0.1)
    assert calculateBonuses("Premium", 10000) == __mult(1.5, 0.1)
    assert calculateBonuses("Premium", 50000) == __mult(2, 0.1)
    assert calculateBonuses("Premium", 100000) == __mult(2.5, 0.1)
    assert calculateBonuses("Premium", 100001) == __mult(2.5, 0.1)


def test_diamond():
    assert calculateBonuses("Diamond", 1) == __mult(1, 0.2)
    assert calculateBonuses("Diamond", 10000) == __mult(1.5, 0.2)
    assert calculateBonuses("Diamond", 50000) == __mult(2, 0.2)
    assert calculateBonuses("Diamond", 100000) == __mult(2.5, 0.2)
    assert calculateBonuses("Diamond", 100001) == __mult(2.5, 0.2)


def test_nothing():
    assert calculateBonuses("", 1) == __mult(1, 0.0)
    assert calculateBonuses("nothing", 10000) == __mult(1.5, 0.0)
    assert calculateBonuses("anothing", 50000) == __mult(2, 0.0)
    assert calculateBonuses("znothing", 100000) == __mult(2.5, 0.0)
    assert calculateBonuses(".nothing", 100001) == __mult(2.5, 0.0)


def __mult(x, y):
    return x * y
